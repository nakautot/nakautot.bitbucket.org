( function ( _, $ ) {
	var AjaxData       = {};
	var cover          = $( '.resto-cover' );
	var navbar         = $( '.resto-nav' );
	var navbarBrand    = $( '.navbar-brand' );
	var social         = $( '.fa-facebook, .fa-twitter, .fa-instagram' );
	var categoryHolder = $( '.resto-category' );
	var itemHolder     = $( '.resto-item' );
	var navbarHolder   = $( '.navbar-nav' );
	var catImageUpload = $( '.cat-image-upload' );
	var itmImageUpload = $( '.item-image-upload' );

	_.templateSettings.variable = "rc";

	var categoryTemplate      = _.template( $( 'script.category-template' ).html() );
	var itemTemplate          = _.template( $( 'script.item-template' ).html() );
	var navTemplate           = _.template( $( 'script.nav-template' ).html() );
	var adminItemTemplate     = _.template( $( 'script.admin-item' ).html() );
	var adminItemItemTemplate = _.template( $( 'script.admin-item-item' ).html() );
	var adminCategoryTemplate = _.template( $( 'script.admin-table' ).html() );

	function initialize () {
		cover.show();
		navbar.hide();
		categoryHolder.hide();
		itemHolder.hide();
	}

	function displayItems ( items ) {
		itemHolder.html( '' );
		for ( item in items ) {
			var value = items[ item ];
			if ( !value.isHidden ) {
				itemHolder.append( itemTemplate( value ) );
			}
		}
	}

	function displayAdmin () {
		itemHolder.html( adminCategoryTemplate() );
		var table = $( '.admin-category-table' );
		for ( index in AjaxData.category ) {
			if ( index !== '0' ) {
				var category = AjaxData.category[ index ];
				category.hideStatus = category.isHidden ? '-slash' : '';
				table.append( adminItemTemplate( category ) );
			}
		}
		bindToggleVisibleCat();
		bindIndivCategoryData();
	}

	function categoryClickAction ( elem ) {
		var id = String( $( elem ).data( 'id' ) );
		if ( id !== '0' ) {
			var items = _.where( _.values( AjaxData.items ), { 'category' : id } );
			$( '.cat' ).removeClass( 'active' );
			$( '.cat' + id ).addClass( 'active' );
			displayItems( items );
		} else {
			displayAdmin();
		}
	}

	function bindToggleVisibleItem () {
		$( '.toggle-item' ).click( function () {
			commonHide( this, AjaxData.items );
		} );
	}

	function cancelHandler ( editPane ) {
		editPane.hide( 'slow', function () {
			$( '.admin-category' ).show( 1000 );
		} );
		$( '.show-edit' ).removeClass( 'fa-chevron-up' );
		$( '.show-edit' ).addClass( 'fa-chevron-down' );
		$( '.cat-btn-grop-new' ).addClass( 'hidden' );
		$( '.cat-btn-grop-old' ).addClass( 'hidden' );
		$( '.admin-category .heading' ).attr( 'contenteditable', false );
		$( '.admin-category .description' ).attr( 'contenteditable', false );

		$( '.all-img' ).unbind( 'click' );
	}

	function showHandler ( editPane, elem ) {
		var id            = String( $( elem ).data( 'id' ) );
		var table         = $( '.admin-item-table' );
		var deleteBtn     = $( '.delete-edit' );
		var newItemBtn    = $( '.new-item' );
		var newItemPane   = $( '.item-btn-grop-new' );
		var newCatBtnGrp  = $( '.cat-btn-grop-old' );
		var saveItemBtn   = $( '.save-new-item' );
		var cancelItemBtn = $( '.cancel-new-item' );

		table.html( '' );

		$( elem ).closest( 'tr.admin-category' ).removeClass( 'edit-hide' );
		$( '.admin-category.edit-hide' ).hide( 'slow', function () {
			editPane.show( 1000 );
		} );
		$( elem ).closest( 'tr.admin-category' ).addClass( 'edit-hide' );

		for ( index in AjaxData.items ) {
			if ( AjaxData.items[ index ].category === id ) {
				var value        = AjaxData.items[ index ];
				value.id         = index;
				value.hideStatus = value.isHidden ? '-slash' : '';
				table.append( adminItemItemTemplate( value ) );
				itemActions( editPane, value.id, elem );
			}
		}

		bindToggleVisibleItem();
		deleteBtn.data( 'id', id );
		$( '.cat-btn-grop-old' ).removeClass( 'hidden' );

		var heading = $( elem ).closest( '.admin-category' ).find( '.heading' );
		var description = $( elem ).closest( '.admin-category' ).find( '.description' );

		heading.attr( 'contenteditable', true );
		description.attr( 'contenteditable', true );

		heading.focusout( function () {
			AjaxData.category[ $( this ).data( 'id' ) ].name = $( this ).html();
		} );
		description.focusout( function () {
			AjaxData.category[ $( this ).data( 'id' ) ].description = $( this ).html();
		} );

		newItemBtn.click( function () {
			$( '#new-item-name' ).val( '' );
			$( '#new-item-desc' ).val( '' );
			$( '#new-item-price' ).val( '' );
			table.addClass( 'hidden' );
			newItemBtn.addClass( 'hidden' );
			newCatBtnGrp.addClass( 'hidden' );
			newItemPane.removeClass( 'hidden' );
		} );

		cancelItemBtn.click( function () {
			cancelItem( table, newItemBtn, newCatBtnGrp, newItemPane, editPane, elem );
		} );

		saveItemBtn.click( function () {
			AjaxData.items[ String( parseInt( _( _( AjaxData.items ).keys() ).last() ) + 1 ) ] = {
				'name'        : $( '#new-item-name' ).val(),
				'description' : $( '#new-item-desc' ).val(),
				'image'       : 'sample',
				'price'       : $( '#new-item-price' ).val(),
				'category'    : id
			}
			cancelItem( table, newItemBtn, newCatBtnGrp, newItemPane, editPane, elem );
		} );

		uploadCatImage = function ( file ) {
			if ( !file || !file.type.match( /image.*/ ) ) {
				return;
			}

			var fd = new FormData();
			fd.append("image", file);

			var xhr = new XMLHttpRequest();
			xhr.open("POST", "https://api.imgur.com/3/image.json");
			xhr.onload = function() {
				AjaxData.category[ imgCatSel ].image = JSON.parse( xhr.responseText ).data.link;
				displayAdmin();
			}

			xhr.setRequestHeader('Authorization', 'Client-ID 11e07ef777e5c61');
			xhr.send(fd);
		}

		$( '.img' + id ).click( function () {
			imgCatSel = id;
			catImageUpload.click();
		} );
	}

	function cancelItem ( table, newItemBtn, newCatBtnGrp, newItemPane, editPane, elem ) {
		table.removeClass( 'hidden' );
		newItemBtn.removeClass( 'hidden' );
		newCatBtnGrp.removeClass( 'hidden' );
		newItemPane.addClass( 'hidden' );
		showHandler(  editPane, elem  );
	}

	function itemActions ( editPane, id, elem ) {
		var deleteItemBtn = $( '.delete-item' );
		var editName      = $( '.item-name-edit' );
		var editPrice     = $( '.item-price-edit' );
		var editDesc      = $( '.item-desc-edit' );

		deleteItemBtn.click( function () {
			delete AjaxData.items[ String( $( this ).data( 'id' ) ) ];
			cancelHandler( editPane );
		} );

		editName.focusout( function () {
			AjaxData.items[ $( this ).data( 'id' ) ].name = $( this ).html();
		} );

		editPrice.focusout( function () {
			AjaxData.items[ $( this ).data( 'id' ) ].price = $( this ).html();
		} );

		editDesc.focusout( function () {
			AjaxData.items[ $( this ).data( 'id' ) ].description = $( this ).html();
		} );

		uploadItemImage = function ( file ) {
			if ( !file || !file.type.match( /image.*/ ) ) {
				return;
			}

			var fd = new FormData();
			fd.append("image", file);

			var xhr = new XMLHttpRequest();
			xhr.open("POST", "https://api.imgur.com/3/image.json");
			xhr.onload = function() {
				AjaxData.items[ imgItmSel ].image = JSON.parse( xhr.responseText ).data.link;
				console.log(AjaxData.items[ id ])
				showHandler( editPane, elem )
			}

			xhr.setRequestHeader('Authorization', 'Client-ID 11e07ef777e5c61');
			xhr.send(fd);
		}

		$( '.imgItem' + id ).click( function () {
			imgItmSel = id;
			itmImageUpload.click();
		} );
	}

	var showHideItems = {
		'show' : {
			'remove'  : 'fa-chevron-up',
			'add'     : 'fa-chevron-down',
			'handler' : cancelHandler
		},
		'hide' : {
			'remove'  : 'fa-chevron-down',
			'add'     : 'fa-chevron-up',
			'handler' : showHandler
		}
	}

	function bindIndivCategoryData () {
		var editPane = $( '.jumbotron' );

		editPane.hide();

		$( '.new-cat' ).click( function () {
			$( '.admin-category' ).hide( 'slow', function () {
				editPane.show( 1000 );
			} );
			$( '.admin-item-table' ).html( '' );
			$( '.cat-btn-grop-new' ).removeClass( 'hidden' );
			$( '.cat-btn-grop-old' ).addClass( 'hidden' );
			$( '.new-item' ).addClass( 'hidden' );
		} );
		$( '.save-edit' ).click( function () {
			AjaxData.category[ String( parseInt( _( _( AjaxData.category ).keys() ).last() ) + 1 ) ] = {
				'name'        : $( '#new-cat-name' ).val(),
				'description' : $( '#new-cat-desc' ).val(),
				'image'       : 'sample'
			}
			displayAdmin ();
			processData();
			$( '.new-item' ).removeClass( 'hidden' );
		} );
		$( '.cancel-edit' ).click ( function () {
			cancelHandler( editPane );
			$( '.new-item' ).removeClass( 'hidden' );
		} );
		$( '.delete-edit' ).click ( function () {
			delete AjaxData.category[ String( $( this ).data( 'id' ) ) ];
			displayAdmin ();
			processData();
		} );
		$( '.edit-control' ).click( function () {
			var icon = $( this ).find( '.show-edit' );
			var stat = showHideItems[ icon.hasClass( 'fa-chevron-up' ) ? 'show' : 'hide' ];

			icon.removeClass( stat.remove );
			icon.addClass( stat.add );

			stat.handler( editPane, this );
		} );
		$( '.sync-cat' ).click( function () {
			$( '.sync-cat' ).addClass( 'disabled' );
			$( '.sync-cat .fa.fa-spinner.fa-spin' ).removeClass( 'hidden' );
			$.ajax( {
				url         : "https://api.mongolab.com/api/1/databases/resto/collections/data?apiKey=nsAN6PXLfvsWgqrIFcziVcKBl1Pm4BDP",
		  		data        : JSON.stringify( AjaxData ),
		  		type        : "POST",
		  		contentType : "application/json",
		  		'success'  : function ( data ) {
		  			$( '.sync-cat' ).removeClass( 'disabled' );
		  			$( '.sync-cat .fa.fa-spinner.fa-spin' ).addClass( 'hidden' );
				},
				'error'    : function ( error ) {
					alert( error );
					$( '.sync-cat' ).removeClass( 'disabled' );
					$( '.sync-cat .fa.fa-spinner.fa-spin' ).addClass( 'hidden' );
				}
		  	} );
		} );
		$( '.reset-cat' ).click( function () {
			$( '.reset-cat' ).addClass( 'disabled' );
			$( '.reset-cat .fa.fa-spinner.fa-spin' ).removeClass( 'hidden' );
			$.ajax( {
				'dataType' : 'json',
				'url'      : 'https://api.mongolab.com/api/1/databases/resto/collections/data?apiKey=nsAN6PXLfvsWgqrIFcziVcKBl1Pm4BDP',
				'success'  : function ( data ) {
					AjaxData = data[ 0 ];
					processData();
					displayAdmin();
					$( '.reset-cat' ).removeClass( 'disabled' );
					$( '.reset-cat .fa.fa-spinner.fa-spin' ).addClass( 'hidden' );
				},
				'error'    : function ( error ) {
					console.log( error )
					$( '.reset-cat' ).removeClass( 'disabled' );
					$( '.reset-cat .fa.fa-spinner.fa-spin' ).addClass( 'hidden' );
				}
			} );
		} );
	}

	function commonHide ( elemRaw, collection ) {
		var elem   = $( elemRaw );
		var id     = elem.attr( 'data-id' );
		var btn    = elem.find( '.fa.fa-eye, .fa.fa-eye-slash' );
		var prefix = btn.hasClass( 'fa-eye' ) ? {
			'add'      : '-slash',
			'sub'      : '',
			'isHidden' : true
		} : {
			'add' : '',
			'sub' : '-slash',
			'isHidden' : false
		}
		btn.removeClass( 'fa-eye' + prefix.sub );
		btn.addClass( 'fa-eye' + prefix.add );
		collection[ Number( id ) ].isHidden = prefix.isHidden;
	}

	function bindToggleVisibleCat () {
		$( '.toggle-cat' ).click( function () {
			commonHide( this, AjaxData.category );
			processData();
		} );
	}

	function bindCategoryClick () {
		$( '.item-display' ).click( function () {
			categoryClickAction( this );
			categoryHolder.hide( 'slow' );
			navbar.show( 1000 );
			itemHolder.show();
		} );
	}

	function bindCategoryNavClick () {
		$( '.cat' ).click( function () {
			itemHolder.hide( 'slow', _.bind( function () {
				categoryClickAction( this );
				itemHolder.show( 1000 );
			}, this ) );
		} );
	}

	function processData () {
		categoryHolder.html( '' );
		navbarHolder.html( '' );
		for ( var key in AjaxData.category ){
			var value   = AjaxData.category[ key ];
			value.id    = key;
			value.image = value.image || AjaxData.items[ value.top ].image;
			if ( !value.isHidden ) {
				categoryHolder.append( categoryTemplate( value ) );
				navbarHolder.append( navTemplate( value ) );
			}
		}
		bindCategoryClick();
		bindCategoryNavClick();
	}

	function fetchData () {
		$.ajax( {
			'dataType' : 'json',
			'url'      : 'https://api.mongolab.com/api/1/databases/resto/collections/data?apiKey=nsAN6PXLfvsWgqrIFcziVcKBl1Pm4BDP',
			'success'  : function ( data ) {
				AjaxData = data[ 0 ];
				processData();
			},
			'error'    : function ( error ) {
				console.log( error )
			}
		} );
	}

	cover.click( function ( e ) {
		e.preventDefault();
		cover.hide( 'slow' );
		categoryHolder.show( 1000 );
	} );
	social.click( function ( e ) {
		e.stopPropagation();
	} );
	navbarBrand.click( function ( e ) {
		e.preventDefault();
		initialize();
	} );

	initialize();
	fetchData();

} )( _, $ );
